
from collections import defaultdict
from queue import PriorityQueue
import math
import numpy as np
from typing import List

class Graph:

	def __init__(self):
		self.grafo = defaultdict(list)
		self.posicao = np.empty(shape=(0,2))

	def addEdge(self,u,v, posX,posY):
		self.grafo[u].append(v)
		self.posicao = np.append(self.posicao, [[posX,posY]], axis =0)

	def heuristica(self, a, b):
		x1 = self.posicao[a][0]
		y1 = self.posicao[a][1]
		x2 = self.posicao[b][0]
		y2 = self.posicao[b][1]
		
		return math.sqrt((x1 - x2)**2 + (y1- y2)**2)
		
	def cost(self, a, b):
		return self.heuristica(a,b)


	def aSTAR(self, vInicial, vFinal, path:List[int]=[]):

		fila = PriorityQueue()
		fila.put(vInicial, 0)
		

		custoAtual = {}
		origem = {}
		origem[vInicial] = None
		custoAtual[vInicial] = 0
		
		while not fila.empty():

			current = fila.get()
			
			if current == vFinal:
				break

		for next in self.grafo[current]:
					novoCusto = self.heuristica(next,vFinal)+self.heuristica(current, next)
					
					if next not in custoAtual or novoCusto < custoAtual[next]:
						custoAtual[next] = novoCusto
						prioridade = novoCusto + self.heuristica(vFinal, next)
						print(prioridade)
						fila.put(next, prioridade)
						origem[next] = current
						
					
		
		path = path + [vInicial]
		if self.grafo[vInicial] == self.grafo[vFinal]:
			return path
		
		
		for node in self.grafo[vInicial]:
			if node not in path:
				newpath = self.aSTAR(node, vFinal, path)
				if newpath: return newpath
		return None
						


g = Graph()
g.addEdge(0, 1, -2, -2)
g.addEdge(0, 2, -2, -2)
g.addEdge(1, 3, -1, -1)
g.addEdge(1, 4, -1, -1)
g.addEdge(1, 5, -1, -1)
g.addEdge(2, 3, -3, 1)
g.addEdge(3, 8, -1, 2)
g.addEdge(4, 8, 2, 1)
g.addEdge(5, 6, 2, -3)
g.addEdge(6, 7, 4, -1)
g.addEdge(7, 8, 4, 4)
g.addEdge(8, 6, 3, 3)


print(g.aSTAR(0, 5))