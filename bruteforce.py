from collections import defaultdict
import numpy as np
import math

class Graph:

    def __init__(self):
        self.graph = defaultdict(list)
        self.posicao = defaultdict(list)
        print(self.graph)
        
    def addEdge(self,u,v):
        self.graph[u].append(v)
        self.graph[v].append(u)
        
    def addVertex(self, v, pos = []):
         self.graph[v] = []
         self.posicao[v] = pos
         return v

    def getDistancia(self, a, b):
        d = 0     
        for i in range(0,len(self.posicao[a])): 
            d += (self.posicao[a][i] - self.posicao[b][i])**2
        
        return math.sqrt(d)

    def bruteForce(self, v):
        distancia = [0] * (len(self.graph))
        menorDistancia = 0
        menorVizinho = v

        for i in self.graph:
		    
            distancia[i] = self.getDistancia(i, v)

            if menorDistancia == 0:
                menorDistancia = distancia[i]
            else:
                if distancia[i] < menorDistancia and distancia[i] > 0:
                    menorDistancia = distancia[i]
                    menorVizinho = i
        
        return menorVizinho




if __name__ == "__main__":

	g = Graph()
	v0 = g.addVertex(0, [-2,-2,3])
	v1 = g.addVertex(1, [-1,-1,2])
	v2 = g.addVertex(2, [-3,1,5])
	v3 = g.addVertex(3, [-1,2,6])
	v4 = g.addVertex(4, [2,1,3])
	v5 = g.addVertex(5, [2,-3,2])
	v6 = g.addVertex(6, [4,-1,9])
	v7 = g.addVertex(7, [4,4,10])
	v8 = g.addVertex(8, [3,3,0])


	g.addEdge(v0, v1)
	g.addEdge(v0, v2)
	g.addEdge(v1, v3)
	g.addEdge(v1, v4)
	g.addEdge(v1, v5)
	g.addEdge(v2, v3)
	g.addEdge(v3, v8)
	g.addEdge(v4, v8)
	g.addEdge(v5, v6)
	g.addEdge(v6, v7)
	g.addEdge(v7, v8)
	g.addEdge(v8, v6)

	print(g.bruteForce(v2))